<!DOCTYPE html>
<html lang="cs">

<head>
    <!-- Specifikování mobilních zařízení  -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Ikonka stránky -->
    <link rel="shortcut icon" href="./public/fav.png">
    <!-- Autor -->
    <meta name="author" content="Romana Kittnerová">
    <!-- Popis -->
    <meta name="description" content="">
    <!-- Charset -->
    <meta charset="UTF-8">
    <!-- Titulek -->
    <title>RomankaStranky</title>
    <!-- GOOGLE FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Oswald:300,500,600|Roboto:400,700" rel="stylesheet">
    <!-- FONT AWESOME -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
        integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <!-- CSS -->
    <link rel="stylesheet" href="./src/css/main.css">
</head>

<body>
    <nav>
    </nav>
    <section>
        <form class="form-dual">
            <div class="left-form">
                <label for="nadpis">Nadpis</label>
                <input class="input" id="nadpis" type="text" name="nadpis" required>
                <br />
                <label for="podnadpis">Podnadpis</label>
                <input class="input" id="podnadpis" type="text" name="podnadpis">
                <br />
                <textarea class="text-input" class="text" id="text" type="textarea" name="text" cols="21"
                    rows="8"></textarea>
            </div>
            <div class="right-form">
                <label for="fotka">Fotka</label>
                <input class="input" id="fotka" type="file" name="fotka" accept="image/x-png,image/gif,image/jpeg" />
                <br />
                <input class="input" type="submit" value="Publikovat">
            </div>
        </form>
    </section>
    <footer>
        <p>Copyright &copy;
            <script>document.write(new Date().getFullYear());</script> | Made
            with ❤︎ by <a>Romana Kittnerová</a></p>
    </footer>
</body>
<html>